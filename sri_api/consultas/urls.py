from django.urls import path
from .views import ConsutalRuc
urlpatterns = [
    path('<ruc>', ConsutalRuc.as_view()),
]
