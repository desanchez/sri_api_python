from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
import requests
from lxml import etree
from rest_framework import status

# Create your views here.
class ConsutalRuc(APIView):

    def get(self, request,ruc):
        r = requests.get("https://srienlinea.sri.gob.ec/facturacion-internet/consultas/publico/ruc-datos2.jspa?accion=siguiente&ruc="+ruc)
        result = r.text
        find = result.find('<table class="formulario">')
        lista = []
        tipo = None
        if find != -1:
            result = result[find:]
            find_tb = result.find('</table>')
            if find_tb != -1:
                result = result[:find_tb]
                result += '</table>'
            find_a = result.find('<a')
            if find_a != -1:
                find_a_c = result.find('</a>')
                result_final_a = result[find_a:find_a_c]
                result_sociedad_int = result_final_a.find('>')
                tipo = result_final_a[result_sociedad_int+1:]
                print(tipo)
            table = etree.HTML(result).find("body/table")
            rows = iter(table)
            headers = [col.text for col in next(rows)]
            for row in rows:
                values = [col.text for col in row]
                if len(values)>1:
                    if values[0] != 'None':
                        if values[0] == 'Tipo de Contribuyente':
                            if tipo:
                                lista.append({'key':values[0],'value':tipo})
                            else:
                                lista.append({'key':values[0],'value':values[1]})

                        else:
                            lista.append({'key':values[0],'value':values[1]})
        return Response({'detalle':lista},status=status.HTTP_200_OK)
